
local database = xiDatabase()
if database then

	local stats = {"cash","points","wins","toptimes"}
	local users = {"Krzyszof","Xiti","SlowSheep","Kucky","Frankaa"}
	for i,user in pairs(users) do

		local data = database:get("user",user) or {}
		for _,dataname in pairs(stats) do

			if not data[dataname] then
				data[dataname] = 0
			end

			data[dataname] = data[dataname] + math.random(10,50)
		end

		database:set("user",user,data)
	end

	users = nil


	-- Get & display stats
	local users = database:rawget("user")
	if users then

		for username,data in pairs(users) do

			local string = username..": "
			for i,value in pairs(data) do
				string = string.."["..i.."]: "..value.."   "
			end

			outputChatBox(string,getRootElement(),255,255,255,true)
		end
	end

	outputChatBox(string.rep("_",60),getRootElement(),255,255,255,true)
end