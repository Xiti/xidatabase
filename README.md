
## xiDatabase storage for MTA:SA 

xiDatabase its SQLite library what makes managing SQLite or MySQL very easy in MTA:SA modification.  
Library also store whole table in memory so real time modifications directly in SQL won't change data what is cached by this library.

## Open example.lua to learn how to use it or check syntax below
```lua
// Initalize instance of xiDatabase file with default table schema and SQLite

xiDatabase()


// You can also use your own database it may be MySQL or SQLite,
// but you have to keep table schema

xiDatabase(dbConnect("sqlite",":/file.db","some_table"),"table_name")

// Set/get/rawget data

local database = xiDatabase()
if database then
    database:set("parent,"key","some_value")    // Returns - true
    database:get("parent,"key")                 // Returns "some_value"
    database:rawget("parent")                   // Return array - {key = "some_value"}
end

```

###### Copyright by Xiti 2018 & https://www.TheCrewGaming.com
