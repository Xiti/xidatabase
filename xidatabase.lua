
xiDatabase = {}

do
	xiDatabase.__index = xiDatabase
	setmetatable(xiDatabase,xiDatabase)
end

function xiDatabase.__call(self,...)

	local args = {...}
	if #args == 0 then
		return self.init()
	else
		return self.new(unpack(args))
	end
end

function xiDatabase.init()

	local schema = {
		{"parent" 	,"VARCHAR(255)"},
		{"key" 		,"VARCHAR(255)"},
		{"value" 	,"TEXT DEFAULT ''"},
	}

	local table_name = "xidatabase"
	local schema_string = "CREATE TABLE IF NOT EXISTS `??`("
	for i,value in pairs(schema) do

		schema_string = schema_string.."`"..value[1].."` "..value[2]
		if #schema == i then
			schema_string = schema_string..");"
		else
			schema_string = schema_string..","
		end
	end

	local connection_type = "sqlite"
	local connection_string = ":/xidatabase.db"

	local connection = dbConnect(connection_type,connection_string)
	if not connection then
		return error("xiDatabase.init - Cannot connect to database.")
	else
		dbExec(connection,schema_string,table_name)
	end

	return xiDatabase(connection,table_name)
end

function xiDatabase.new(connection,table_name)

	local self = setmetatable({},xiDatabase)
	do
		self.connection = connection
		self.table_name = table_name
		self.storage = {}
	end

	local query = dbQuery(self.connection,"SELECT * FROM `??`",self.table_name)
	if query then

		local result,rows = dbPoll(query,-1)
		if result and rows > 0 then

			for result,row in pairs(result) do

				local parent = tostring(row.parent)
				if not self.storage[parent] then
					self.storage[parent] = {}
				end

				local key = tostring(row.key)
				local value = tostring(row.value)
				if key and value then
					self.storage[parent][key] = fromJSON(value)
				end
			end
		end

		return self
	end

	return error("xiDatabase.new - Cannot load database with table name: "..self.table_name)
end

function xiDatabase:set(parent,key,value)

	if parent and key then

		local parent = tostring(parent)
		local key = tostring(key)
		if not self.storage[parent] then
			self.storage[parent] = {}
		end
		
		if type(value) ~= "table" and value == self.storage[parent][key] then 
			return true
		end
		
		do
			self.storage[parent][key] = value
		end
		
		dbExec(self.connection,"DELETE FROM `??` WHERE `parent`=? AND `key`=?;",self.table_name,parent,key)
		if value ~= nil then
			dbExec(self.connection,"INSERT INTO `??`(`parent`,`key`,`value`) VALUES (?,?,?);",self.table_name,parent,key,toJSON(value))
		end
		
		return true
	else
		return false
	end
end

function xiDatabase:rawget(parent,key)

	if parent == nil then
		return self.storage
	elseif key == nil then
		return self.storage[tostring(parent)]
	else

		local parent = tostring(parent)
		if not self.storage[parent] then
			return false
		else
			return self.storage[parent][tostring(key)]
		end		
	end
end


function xiDatabase:get(parent,key)

	if parent and key then

		local parent = tostring(parent)
		if not self.storage[parent] then
			return false
		else
			return self.storage[parent][tostring(key)]
		end
	else
		return false
	end
end